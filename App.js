import { StatusBar } from 'expo-status-bar';
import React,  {Component, useState, useEffect} from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';

import Amplify, { Auth } from 'aws-amplify'
import awsmobile from './aws-exports'

import SignIn from './src/screens/SignIn';
import SignUp from './src/screens/SignUp';
import ResetPassword from './src/screens/ResetPassword';
import ConfirmSignUp from './src/screens/ConfirmSignUp';
import Home from './src/screens/Home';
import UserInfo from './src/screens/UserInfo';
import DeviceList from './src/screens/DeviceList';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

Amplify.configure(awsmobile)

const AuthenticationStack = createStackNavigator();
const AppStack = createStackNavigator();
const AuthenticationNavigator = props => {
	return (
	  <AuthenticationStack.Navigator headerMode="none">
		<AuthenticationStack.Screen name="SignIn">
		  {screenProps => (
			<SignIn {...screenProps} updateAuthState={props.updateAuthState} />
		  )}
		</AuthenticationStack.Screen>
		<AuthenticationStack.Screen name="SignUp" component={SignUp} />
		<AuthenticationStack.Screen name="ConfirmSignUp" component={ConfirmSignUp} />
		<AuthenticationStack.Screen name="ResetPassword" component={ResetPassword} />

	  </AuthenticationStack.Navigator>
	);
};

const AppNavigator = props => {
	return (
		<AppStack.Navigator>
			<AppStack.Screen name="Home">
				{screenProps => (
					<Home {...screenProps} updateAuthState={props.updateAuthState} />
				)}
			</AppStack.Screen>

			<AppStack.Screen name="UserInfo">
				{screenProps => <UserInfo {...screenProps} userInfo={props.userInfo} updateAuthState={props.updateAuthState} />}
			</AppStack.Screen>

			<AppStack.Screen name="DeviceList" component={DeviceList} />
		</AppStack.Navigator>
	);
};

const Initializing = () => {
	return (
	  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
		<ActivityIndicator size="large" />
	  </View>
	);
};

function App() {
	const [isUserLoggedIn, setUserLoggedIn] = useState('initializing');
	const [userInfo, setUserInfo] = useState({})
	useEffect(() => {
		checkAuthState();
	}, []);
	
	async function checkAuthState() {
		try {
			await Auth.currentAuthenticatedUser();
			console.log('<img draggable="false" class="emoji" alt="✅" src="https://s.w.org/images/core/emoji/11/svg/2705.svg"> User is signed in');
			setUserLoggedIn('loggedIn');

			const user = await Auth.currentUserInfo();
			console.log("USER", user);
			setUserInfo(user.attributes)
		} catch (err) {
			console.log('<img draggable="false" class="emoji" alt="❌" src="https://s.w.org/images/core/emoji/11/svg/274c.svg"> User is not signed in');
			setUserLoggedIn('loggedOut');
		}
	}

	function updateAuthState(isUserLoggedIn) {
		setUserLoggedIn(isUserLoggedIn);
	}

	return (
		<NavigationContainer>
			{isUserLoggedIn === 'initializing' && <Initializing />}
			{isUserLoggedIn === 'loggedIn' && (
			<AppNavigator userInfo={userInfo} updateAuthState={updateAuthState} />
			)}
			{isUserLoggedIn === 'loggedOut' && (
				<AuthenticationNavigator updateAuthState={updateAuthState} />
			)}
		</NavigationContainer>
    );
}


// export default withAuthenticator(App);
export default App;
