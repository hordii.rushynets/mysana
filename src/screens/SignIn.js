import React, { useState } from 'react';
import { Auth } from 'aws-amplify';
import {TouchableOpacity, View, Alert, Text} from 'react-native';

import {InputFieldsView, Input} from '../components/InputFields';
import {SubmitButton} from '../components';
import {AppNameText} from '../components';
import {AuthTitle} from '../components'
import {Container} from '../components';
import {AuthText} from '../components';
import {Group} from '../components';


export default function SignIn({navigation, updateAuthState}) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    
    async function signIn() {
        try {
            await Auth.signIn(username, password);
            console.log('<img draggable="false" class="emoji" alt="✅" src="https://s.w.org/images/core/emoji/11/svg/2705.svg"> Success');
            updateAuthState('loggedIn');
        } catch (error) {
            console.log('<img draggable="false" class="emoji" alt="❌" src="https://s.w.org/images/core/emoji/11/svg/274c.svg"> Error signing in...', error);

            Alert.alert(
                'Fail Sign In',
                error.message,
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            )
        }
    }

    return(
        // <Group>
        <Container>
            <Group>
            <View style={{
                position: 'absolute',
                top: 34,
                left: 24,
                flexDirection:'row', 
                flexWrap:'wrap'
            }}>
                <AppNameText>mySana</AppNameText>
            </View>

			<View style={{alignItems: 'center'}}>
                <AuthTitle>Log in to your account</AuthTitle>
                <InputFieldsView>
                    <Input 
                        value={username}
                        onChangeText={text => setUsername(text)}
                        placeholder="Username"
                        autoCapitalize="none"
                        keyboardType="email-address"
                        textContentType="emailAddress"
                    />
                    <Input
                        value={password}
                        onChangeText={text => setPassword(text)}
                        placeholder="Password"
                        autoCapitalize="none"
                        autoCorrect={false}
                        secureTextEntry
                        textContentType="password"
                    />
                </InputFieldsView>

                <View style={{top: 542, alignItems: "center"}}>
                    <TouchableOpacity style={{marginBottom: 15}} onPress={() => navigation.navigate('ResetPassword')}>
                        <Text style={{textDecorationLine: 'underline'}}>Forgot your password?</Text>
                    </TouchableOpacity>
                    <SubmitButton width={300} title="Log In" onPress={signIn}/>
                </View>
            </View>

            <TouchableOpacity style={{position: 'absolute', bottom: 48, left: 24}} onPress={() => navigation.navigate('SignUp')}> 
                <AuthText style={{textDecorationLine: 'underline'}}>Registration</AuthText>
            </TouchableOpacity>

            </Group>
          </Container>  
        // </Group>
    )
}
