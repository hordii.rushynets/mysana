import React, { useState } from 'react';
import { Auth } from 'aws-amplify';
import {TouchableOpacity, View, Alert, Text} from 'react-native';

import {InputFieldsView, Input} from '../components/InputFields';
import {SubmitButton} from '../components';
import {AppNameText} from '../components';
import {AuthTitle} from '../components';
import {Container} from '../components';
import {AuthText} from '../components';
import {Group} from '../components';


function ResetPassword({navigation}) {
    const [email, setEmail] = useState('')

    return(
        <Container>
            <Group>
            <View style={{
                position: 'absolute',
                top: 34,
                left: 24,
                flexDirection:'row', 
                flexWrap:'wrap'
            }}>
                <AppNameText>mySana</AppNameText>
            </View>
            
            <View style={{alignItems: 'center', width: '100%'}}>
                <AuthTitle>Reset your password</AuthTitle>
                <View style={{top: 174, width: 272}}>
                    <AuthText>
                    Please enter your email or username 
                    and we will send you the instructions 
                    to reset your password.
                    </AuthText>
                </View>
                <InputFieldsView style={{top: 400}}>
                    <Input
                        value={email}
                        onChangeText={text => setEmail(text)}
                        placeholder="Enter email"
                        autoCapitalize="none"
                        keyboardType="email-address"
                        textContentType="emailAddress"
                    />
                </InputFieldsView>

                <View style={{top: 500, alignItems: "center"}}>
                    <SubmitButton title="Send" onPress={() => console.log("Send button pressed")} />
                </View>
            </View>

            <TouchableOpacity style={{position: 'absolute', bottom: 48, left: 24}} onPress={() => navigation.navigate('SignIn')}> 
                <AuthText style={{textDecorationLine: 'underline'}}>Log In</AuthText>
            </TouchableOpacity>
            </Group>
        </Container>
    )
}

export default ResetPassword;
