import React, { useState } from 'react';
import { Auth } from 'aws-amplify';
import {TouchableOpacity, View, Alert} from 'react-native';

import {InputFieldsView, Input} from '../components/InputFields';
import {SubmitButton} from '../components';
import {AppNameText} from '../components';
import {AuthTitle} from '../components';
import {Container} from '../components';
import {AuthText} from '../components';
import {Group} from '../components';

export default function SignUp({navigation}) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    async function signUp() {
        try {
            await Auth.signUp({ username, password, attributes: { email } });
            console.log('<img draggable="false" class="emoji" alt="✅" src="https://s.w.org/images/core/emoji/11/svg/2705.svg"> Sign-up Confirmed');
            navigation.navigate('ConfirmSignUp');
        } catch (error) {
            console.log('<img draggable="false" class="emoji" alt="❌" src="https://s.w.org/images/core/emoji/11/svg/274c.svg"> Error signing up...', error);

            Alert.alert(
                'Fail Sign Up',
                error.message,
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            )
        }
    }

    return (
        <Container>
        <Group>
            <View style={{
                    position: 'absolute',
                    top: 34,
                    left: 24,
                    flexDirection:'row', 
                    flexWrap:'wrap'
                }}>
                <AppNameText>mySana</AppNameText>
            </View>

			<View style={{alignItems: 'center'}}>
                <AuthTitle>Create account</AuthTitle>
                <InputFieldsView>
                    <Input 
                        value={username}
                        onChangeText={text => setUsername(text)}
                        placeholder="Username"
                        autoCapitalize="none"
                        keyboardType="default"
                        textContentType="none"
                    />

                    <Input
                        value={password}
                        onChangeText={text => setPassword(text)}
                        placeholder="Password"
                        autoCapitalize="none"
                        autoCorrect={false}
                        secureTextEntry
                        textContentType="password"
                    />

                    <Input
                        value={email}
                        onChangeText={text => setEmail(text)}
                        placeholder="Enter email"
                        autoCapitalize="none"
                        keyboardType="email-address"
                        textContentType="emailAddress"
                    />
                </InputFieldsView>
                <View style={{top: 542, alignItems: "center"}}>
                    <SubmitButton title="Create account" onPress={signUp} />
                </View>
            </View>

            <TouchableOpacity style={{position: 'absolute', bottom: 48, left: 24}} onPress={() => navigation.navigate('SignIn')}> 
                <AuthText style={{textDecorationLine: 'underline'}}>Log In</AuthText>
            </TouchableOpacity>
        </Group>
        </Container>
    )
}