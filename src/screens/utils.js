const cryptoJS = require("crypto-js");

const generateDateString = (date) => {
	const year = date.getFullYear().toString();
	
	let month = null;
	
	if(date.getMonth() + 1 < 10) {
	  month = `0${date.getMonth() + 1}`;
	} else {
	  month = `${date.getMonth() + 1}`;
	}
	let day = date.getUTCDate();

	if(day < 10) {
		day = `0${day}`;
	}
  
	const dateString = `${year}${month}${day}`
	return dateString;
}

function getSignatureKey(key, dateStamp, regionName, serviceName) {
    var kDate = cryptoJS.HmacSHA256(dateStamp, "AWS4" + key);
    var kRegion = cryptoJS.HmacSHA256(regionName, kDate);
    var kService = cryptoJS.HmacSHA256(serviceName, kRegion);
    var kSigning = cryptoJS.HmacSHA256("aws4_request", kService);
    return kSigning;
}

function createAmzDate(date) {
	date = date.replace(/:/g, '').slice(0, 17).replace(/-/g, '') + 'Z'
	return date;
}

function generateSignature(canonicalUri, date) {
	const region = "eu-west-1";
	const dateString = generateDateString(date);

	const secretAccessKey = '7oDayx06WUngL4rDi0FMzt2kG2qaLnU7nI/j4Mp/'
	const key = getSignatureKey(secretAccessKey, dateString, region, 'execute-api')
	const amzDate = createAmzDate(date.toISOString());
	
	const payloadHash = `${cryptoJS.enc.Hex.stringify(cryptoJS.SHA256(''))}`;
	
	const canonicalHeaders = `host:02qo27azy5.execute-api.eu-west-1.amazonaws.com\nx-amz-date:${amzDate}\n`;
	const signedHeaders = 'host;x-amz-date';
	const canonicalRequest = `GET\n${canonicalUri}\n${canonicalHeaders}\n${signedHeaders}\n${payloadHash}`;
	const stringToSign =  `AWS4-HMAC-SHA256\n${amzDate}\n${dateString}/eu-west-1/execute-api/aws4_request\n${cryptoJS.enc.Hex.stringify(cryptoJS.SHA256(canonicalRequest))}`;

	const signature = cryptoJS.HmacSHA256(stringToSign, key).toString(cryptoJS.enc.Hex);
	return signature;
}


export {generateDateString};
export {generateSignature};
export {getSignatureKey};
export {createAmzDate};
