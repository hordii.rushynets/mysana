import React, {useState, useEffect} from 'react';
import { View, StyleSheet, Text, AsyncStorage} from 'react-native';
import {Block} from '../components';
import {Group} from '../components';
import {UserBlock} from '../components';
import {Container} from '../components';
import {AppNameText} from '../components';
import {SubmitButton} from '../components';
import {Input} from '../components/InputFields';

import {generateSignature, generateDateString, createAmzDate} from './utils';


import { Auth, API } from 'aws-amplify';

const cryptoJS = require("crypto-js");

export default function Home({navigation, updateAuthState}) {
  const blockText = "We can display all" +
  " available devices using your IP address"
  const [deviceId, setDeviceId] = useState('');
  const [devices, setDevice] = useState([]);

  /*useEffect(() => {
	  (async function(){
		const devices =  await AsyncStorage.getItem('devices') || [];
		setDevice(JSON.parse(devices));
	  })();
  }, []);*/

  useEffect(()=>{
		(async function(){
			await AsyncStorage.setItem('devices', JSON.stringify(devices))
		})();
		navigation.navigate('DeviceList', {devices: devices});
  }, [devices])

  async function signOut() {
    try {
		await Auth.signOut();
		updateAuthState('loggedOut');
    } catch (error) {
		console.log('Error signing out: ', error);
    }
  }

  function getDeviceByid() {
	const apiName = 'sana-mobile';
	const path = `device/${deviceId}`;

	const canonicalUri = `/dev/device/${deviceId}\n`; 
	const date = new Date();
	const dateString = generateDateString(date);
	const amzDate = createAmzDate(date.toISOString());

	const signature = generateSignature(canonicalUri, date);
		
	const myInit = { // OPTIONAL
		headers: {
			Authorization: `AWS4-HMAC-SHA256 Credential=AKIA4DZDC63CU6WECME5/${dateString}/eu-west-1/execute-api/aws4_request, SignedHeaders=host;x-amz-date, Signature=${signature}`,
			'X-Amz-Date': amzDate,
		}, 
		response: true,
	};
	
	return API.get(apiName, path, myInit)
  }

  const updateDeviceType = (type, serial, deviceList) => {
	  console.log('updateDeviceType1', devices.length);
	  
	  let deviceFound = false;

	  for(let device of deviceList) {
		  if(device.serial_number === serial) {
			  console.log(`${device.serial_number} - ${device.type}`)
			  device.type = type;
			  deviceFound = true;
		  }
	  }
	  if(deviceFound) {
		  setDevice([...deviceList]); 
	  }
	  console.log('updateDeviceType2', devices.length);
  }

  return (
    <Container>
		<Group>
			<View style={{
				marginRight: 24,
				marginTop: 24, 
				flexDirection:'row', 
				flexWrap:'wrap'
			}}>
				<AppNameText>mySana</AppNameText>
				<UserBlock title="Hordii" onPress={() => navigation.navigate('UserInfo')} />
			</View>

			<View style={{alignItems: 'center'}}>
				<Block btnTitle="Get devices" onPress={() => navigation.navigate('DeviceList')} text={blockText}/>
				<View style={styles.inputView}>
					<Input 
						value={deviceId}
						onChangeText={text => setDeviceId(text)}
						placeholder="Device ID"
						textContentType="text"
					/>
				</View>
				<Text style={styles.text}>
					Or you can see available devices after you enter your device ID
				</Text>
				<View style={{top: 520, alignItems: "center"}}>
					<SubmitButton title="Find device" onPress={() => {
							let deviceFound = false;

							for(let device of devices) {
								if(device.serial_number == deviceId) {
									deviceFound = true;
								} 
							}

							if(!deviceFound) {
								const data = (async function(){
									// 619263299450 valid device id
									const response = await getDeviceByid();
									return response;
								})();
								data.then(response => {
									// navigation.navigate('DeviceList', {device: response.data.device});
									if(response.status == '200') {
										
									}
									setDevice([...devices, Object.assign(response.data.device, {type: 'diagnostics', updateDeviceType: updateDeviceType})])	
								}).catch(error => {
									console.log(error);
								})
							} else {
								navigation.navigate('DeviceList', {devices: devices});
							}
						}}/>
				</View>
			</View>
		</Group>
    </Container>
  );
}

const styles = StyleSheet.create({
	inputView: {
		top: 432,
		right: 34
	},
	text: { 
		position: 'absolute',
		width: '100%',
		height: 40,
		left: 24,
		top: 360,

		fontStyle: "normal",
		fontWeight: "normal",
		fontSize: 16,
		lineHeight: 28,

		color: "#283547"
	}
})