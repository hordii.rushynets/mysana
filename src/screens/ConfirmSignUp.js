import { Button, Alert } from 'react-native';
import React, { useState } from 'react';
import { Auth } from 'aws-amplify';

import {InputFieldsView, Input} from '../components/InputFields';
import {AppNameText} from '../components';
import {AuthTitle} from '../components/';
import {UserBlock} from '../components';
import {Container} from '../components';
import {Group} from '../components/';


export default function ConfirmSignUp({ navigation }) {
    const [authCode, setAuthCode] = useState('');
    async function confirmSignUp() {
      try {
        await Auth.confirmSignUp(username, authCode);
        console.log('<img draggable="false" class="emoji" alt="✅" src="https://s.w.org/images/core/emoji/11/svg/2705.svg"> Code confirmed');
        navigation.navigate('SignIn');
      } catch (error) {
        console.log(
          '<img draggable="false" class="emoji" alt="❌" src="https://s.w.org/images/core/emoji/11/svg/274c.svg"> Verification code does not match. Please enter a valid verification code.',
          error.code
        );

        Alert.alert(
          'Fail confirmation',
          error.message,
          [
            { text: "OK", onPress: () => console.log("OK Pressed") }
          ]

        );
      }
    }

    return (
      <Container>
        <Group>
        <View style={{
            marginRight: 24,
            marginTop: 24, 
            flexDirection:'row', 
            flexWrap:'wrap'
        }}>
            <AppNameText>mySana</AppNameText>
        </View>

			  <View style={{alignItems: 'center'}}>
          <AuthTitle>Confirm Sign Up</AuthTitle>
          <Button title="Confirm" color="tomato" onPress={confirmSignUp} />
          <InputFieldsView>
            <Input
                value={authCode}
                onChangeText={text => setAuthCode(text)}
                placeholder="Enter verification code"
                keyboardType="email-address"
            />
          </InputFieldsView>
        </View>
        </Group>
      </Container>
    );
  }