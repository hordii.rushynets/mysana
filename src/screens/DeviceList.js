import React, {useState, forwardRef, useRef, useImperativeHandle} from 'react';
import { View } from 'react-native'; 
import { API } from 'aws-amplify';


import {UserBlock} from '../components';
import {Container} from '../components';
import {DeviceBlock} from '../components'
import {AppNameText} from '../components';
import {ScrollGroup} from '../components'
import {SubmitButton} from '../components';
import {generateSignature, generateDateString, createAmzDate} from './utils';

const DeviceList = (props) => {
    const devices = [...props.route.params.devices];

    function getDeviceRCU (serial_number) {
        const apiName = 'sana-mobile';
        const path = `device/${serial_number}/rcu`;

        const canonicalUri = `/dev/device/${serial_number}/rcu\n`; 
        const date = new Date();
        const dateString = generateDateString(date);
        const amzDate = createAmzDate(date.toISOString());

        const signature = generateSignature(canonicalUri, date);
            
        const myInit = { // OPTIONAL
            headers: {
                Authorization: `AWS4-HMAC-SHA256 Credential=AKIA4DZDC63CU6WECME5/${dateString}/eu-west-1/execute-api/aws4_request, SignedHeaders=host;x-amz-date, Signature=${signature}`,
                'X-Amz-Date': amzDate,
            }, 
            response: true,
        };
        
        return API.get(apiName, path, myInit);
    }

    console.log(devices.length)
    return(
        <Container>
            <ScrollGroup>
                <View style={{
                    marginRight: 24,
                    marginTop: 24, 
                    flexDirection:'row', 
                    flexWrap:'wrap'
                }}>
                    <AppNameText>mySana</AppNameText>
                    <UserBlock title="Hordii" onPress={() => props.navigation.navigate('UserInfo')} />
                </View>
                <View style={{alignItems: 'center'}}>
                    <View style={{top: 52}}>
                        <SubmitButton title="Diagnostics all devices" onPress={() => {console.log("Diagnostics all devices button pressed!!!")}} />
                    </View>
                    <View style={{top: 87, width: '100%'}}>
                        {
                            devices.map(device => {
                                const statusRef = useRef();

                                return(
                                    <DeviceBlock 
                                        type={device.type} 
                                        title={device.serial_number} 
                                        description="Embedded Wi-Fi 6. Supporting HEVC and AV1 standards for ultimate 4K content delivery."
                                        onPress={()=>{
                                            statusRef.current.updateDeviceType('inProgress')
                                            device.updateDeviceType('inProgress', device.serial_number, devices);
                                            const data = (async function(){
                                                // 619263299450 valid device id
                                                const response = await getDeviceRCU(device.serial_number);
                                                return response;
                                            })();
                                            data.then(response => {
                                                if(response.status == 200){
                                                    device.updateDeviceType('diagnosed', device.serial_number, devices);
                                                }
                                                statusRef.current.updateDeviceType('diagnosed')
                                            }).catch(error => {
                                                console.log(error);
                                            })
                                        }}
                                        ref={statusRef}
                                    />
                                )
                            })
                        }
                        
                        {/* <DeviceBlock 
                            type="diagnosed" 
                            title="Far field UHD STB" 
                            description="Embedded Wi-Fi 6. Supporting HEVC and AV1 standards for ultimate 4K content delivery."
                        />
                        <DeviceBlock 
                            type="diagnostics" 
                            title="Far field UHD STB" 
                            description="Embedded Wi-Fi 6. Supporting HEVC and AV1 standards for ultimate 4K content delivery."
                        />
                        <DeviceBlock 
                            type="diagnostics" 
                            title="Far field UHD STB"
                            description="Embedded Wi-Fi 6. Supporting HEVC and AV1 standards for ultimate 4K content delivery."
                        />
                        <DeviceBlock 
                            type="inProgress"
                            title="Far field UHD STB"
                            description="Embedded Wi-Fi 6. Supporting HEVC and AV1 standards for ultimate 4K content delivery."
                        /> */}

                        <View style={{height: 135}}>

                        </View>
                    </View>
                </View>
            </ScrollGroup>
        </Container>
    )
}

export default DeviceList;