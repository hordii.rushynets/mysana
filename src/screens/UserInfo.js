import { Auth, strike } from 'aws-amplify'

import {Text, View, TouchableOpacity} from 'react-native';
import styled from 'styled-components/native';
import React, { useState, useEffect } from 'react';

import {Group} from '../components';
import {Container} from '../components';
import Icon from 'react-native-vector-icons/EvilIcons';

import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

const Block = styled.View`
    top: 124px;
    margin-left: 24px;
    margin-bottom: 48px;
`;

const StyledText = styled.Text`
    margin-bottom: 20px;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 30px;

    color: #FFFFFF;
`;

const LogOutButton = styled.TouchableOpacity`
    position: absolute;
    justify-content: center;

    width: 60px;
    height: 25px;
    left: 24px;
    bottom: 5%;
    margin-left: 24px;
`;

const DeleteAccountButton = styled.TouchableOpacity`
    width: 100px;
    height: 25px;
    justify-content: center;
`

const UserInfo = (props) => {
    async function signOut() {
        try {
            await Auth.signOut();
            props.updateAuthState('loggedOut')
        } catch (error) {
            console.log(error);
        }
    }


    return(
        <Container>
            <Group style={{backgroundColor: "#3B7EE2"}}>
                <TouchableOpacity style={{left: 29, top: 29}} onPress={() => props.navigation.navigate('Home')}>
                    <Icon name="close" size={20} color="#FFFFFF"/>
                </TouchableOpacity>
                <Block>
                    <StyledText style={{fontSize: 24}}>{props.userInfo.email}</StyledText>
                    <StyledText>{props.userInfo.email}</StyledText>
                </Block>
                <Block>
                    <StyledText>France</StyledText>
                    <StyledText>{props.userInfo.phone_number}</StyledText>
                </Block>
                <Block>
                    <DeleteAccountButton>
                        <Text style={{color: "#FFFFFF", fontSize: 14, textDecorationLine: 'underline'}}>Delete account</Text>
                    </DeleteAccountButton>
                </Block>

                <LogOutButton onPress={signOut}>
                    <Text style={{color: "#FFFFFF", fontSize: 16, textDecorationLine: 'underline'}}>Log out</Text>
                </LogOutButton>
            </Group>
        </Container>
    )
}


export default UserInfo;