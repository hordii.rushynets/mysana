import styled from 'styled-components/native';

const Container  = styled.View`
	flex: 1;
	background: rgba(147, 223, 223, 0.16);;
`;

export default Container;