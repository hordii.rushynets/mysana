import styled from 'styled-components/native';
import React from 'react';


const BlockRectangle = styled.View`
    position: absolute;
    width: 312px;
    height: 224px;
    
    top: 92px;

    background: #3B7EE2;
    border-radius: 20px;
`;

const BlockText = styled.Text`
    position: relative;
    width: 264px;
    height: 90px;
    left: 24px;
    top: 22px;

    font-style: normal;
    font-weight: 500;
    font-size: 24px;
    line-height: 30px;

    color: #FFFFFF;
`;

const Button = styled.TouchableOpacity`
    position: relative;
    left: 76px;
    right: 76px;
    top: 40px;
    width: 160px;
    height: 44px;
    background: #EEFAFA;
    border-radius: 100px;
`;

const ButtonText = styled.Text`
    position: absolute;
    left: 12.5%;
    right: 12.5%;
    top: 18.18%;
    bottom: 18.18%;

    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 20px;
    display: flex;
    align-items: center;
    text-align: center;

    color: #3B7EE2;
`;

const BlockButton = (props) => {
    return(
        <Button onPress={props.onPress}>
            <ButtonText>
                {props.title}
            </ButtonText>
        </Button>
    )
}

const Block = (props) => {
    return(
        <BlockRectangle>
            <BlockText>{props.text}</BlockText>
            
            <BlockButton title={props.btnTitle} onPress={props.onPress}/>
        </BlockRectangle>
    )
}

export default Block;