import styled from 'styled-components/native';

const ScrollGroup = styled.ScrollView`
    padding: 0 28px;
    position: absolute;

    width: 100%;
    height: 100%;
    left: 0px;
    top: 0px;

    background: rgba(147, 223, 223, 0.16);
`;

export default ScrollGroup;