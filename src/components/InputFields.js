import styled from 'styled-components/native';

const Input = styled.TextInput`
	width: 312px;
	height: 48px;
	margin-bottom: 28px;

	font-style: normal;
	font-weight: normal;
	font-size: 16px;
	line-height: 20px;

	border: 1px solid rgba(40, 53, 71, 0.2);
	border-radius: 4px;
`;


const InputFieldsView = styled.View`
	position: absolute;
	top: 206px;
	margin-bottom: 32px;
`;


export {InputFieldsView, Input} ;
// export default InputRectangle;
