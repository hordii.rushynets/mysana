import React, {useState, useEffect, forwardRef, useRef, useImperativeHandle} from 'react';
import styled from 'styled-components/native';
import { View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';

/**
 * 
 * @param {string} type diagnosed, diagnostics, inProgress
 */
const DeviceState = ({type}) =>{
    let styles = {
        marginLeft: 16, 
        marginTop: 16,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5,
        paddingLeft: 5,
    };
    let fontSize = 12;
    let fontWeight = 500;
    // font-family: Quicksand;
    let content = '123';

    switch (type) {
        case 'diagnosed':
            styles.backgroundColor = '#4F9F66';
            styles.borderRadius = 4;
            styles.height = 24;
            styles.paddingLeft = 5;
            styles.paddingTop = 2;
            styles.paddingRight = 8;
            styles.width = 93;

            content = 'Diagnosed'
            break;
        
        case 'diagnostics':
            styles.backgroundColor = '#283447';
            styles.borderRadius = 18;
            styles.height = 32;
            styles.paddingLeft = 7;
            styles.paddingRight = 7;
            styles.paddingTop = 16;
            styles.paddingBottom = 16;
            styles.width = 109;
            
            fontSize = 14;
            fontWeight = 600;
            content = 'Diagnostics';
            break

        case 'inProgress':
            styles.backgroundColor = '#3B7EE2';
            styles.borderRadius = 4;
            styles.height = 24;
            styles.paddingLeft = 5;
            styles.paddingBottom = 5;
            styles.paddingTop = 2;
            styles.paddingRight = 8;
            styles.width = 164;
            
            content = 'Diagnostics in progress';

            break

        default:
            break;

    }

    const StateText = styled.Text`
        font-style: normal;
        font-weight: ${fontWeight};
        font-size: ${fontSize}px;
        line-height: 18px;
        color: #FFFFFF;
    `

    const stateStyles = StyleSheet.create({
        stateView: styles
    })

    return (
        <View style={stateStyles.stateView}>
            {type === 'diagnosed' && 
            <>
                <View style={{ width: '50%', position: 'absolute', left: 0}}>
                    <Icon name="check" size={24} color="#FFFFFF" />
                </View>
                <View style={{ left: 24, position: 'absolute'}}>
                    <StateText>{content}</StateText>
                </View>
            </>
            }
            {
                type === 'diagnostics' &&
                <View style={{ position: 'absolute'}}>
                    <StateText>{content}</StateText>
                </View>
            }

            {
                type === 'inProgress' &&
                <>
                    <View style={{ width: '50%', position: 'absolute', left: 0}}>
                        <Icon name="spinner-3" size={24} color="#FFFFFF" />
                    </View>
                    <View style={{ left: 24, position: 'absolute'}}>
                        <StateText>{content}</StateText>
                    </View>
                </>
            }
            
        </View>
    )
}

const Title = styled.Text`
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;
    color: #283547;

    margin-left: 16px;
    margin-top: 20px;
`;

const Description = styled.Text`
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 18px;

    color: #283547;

    margin-left: 16px;
    margin-top: 4px;
`;

const DeviceBlock = forwardRef(({type, title, description, onPress}, ref) => {
    let backgroundColor = "#EEFAFA";
    const [deviceType, setDeviceType] = useState(type);
    useEffect(() => {
    }, [deviceType])

    useImperativeHandle(
        ref,
        () => ({
            updateDeviceType(deviceStatus) {
                setDeviceType(deviceStatus);
            }
        }),
    )
    
    
    if(type === 'diagnosed') {
        backgroundColor = '#FFFFFF'
    }

    const Block = styled.TouchableOpacity`
        width: 100%;
        background: ${backgroundColor};
        height: 136px;
        box-shadow: 0px 0px 12px rgba(40, 52, 71, 0.12);
        border-radius: 20px;
        margin-bottom: 12px;
    `

    return(
        <Block onPress={() =>{
            onPress();
            
        }}>
            <DeviceState type={deviceType}/>
                <Title>{title}</Title>
            <Description>
                {description}
            </Description>
        </Block>
    )
})

export default DeviceBlock;
