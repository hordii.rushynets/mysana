import React from 'react';
import styled from 'styled-components/native';


const Block = styled.TouchableOpacity`
    height: 28px;
    margin-left: 12px;

    padding-top: 5px;
    padding-bottom: 5px;
    padding-left: 10px;
	padding-right: 10px;
    
    padding: 5px 10px;
    background: #3B7EE2;
    border-radius: 100px;
`;

const UsernameText = styled.Text`
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 17px;

    display: flex;
    align-items: center;

    color: #FFFFFF;
`;

const UserBlock = ({ title, onPress }) => {
    return(
        <Block onPress={onPress}>
            <UsernameText>{title}</UsernameText>
        </Block>
    )
}


export default UserBlock