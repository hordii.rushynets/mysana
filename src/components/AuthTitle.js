import styled from 'styled-components/native';


const AuthTitle = styled.Text`
	position: absolute;
	left: 24px;
	top: 124px;
	width: 100%;
	height: 30px;
	
	font-style: normal;
	font-weight: normal;
	font-size: 24px;
	line-height: 30px;

	color: rgba(40, 53, 71, 0.8);
`;

export default AuthTitle;