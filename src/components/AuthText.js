import styled from 'styled-components/native';

const AuthText = styled.Text`
    font-style: normal;
    font-weight: 300;
    font-size: 16px;
    line-height: 20px;

    color: #283547;
`;

export default AuthText;
