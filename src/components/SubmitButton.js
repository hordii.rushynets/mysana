import React from 'react';
import styled from 'styled-components/native';

const Button = styled.TouchableOpacity`
	padding: 7px;
	padding-left: 16px;
	padding-right: 16px;
	height: 44px;
	align-items: center;
	justify-content: center;

	background: #283547;
	border-radius: 100px;
`

const ButtonText = styled.Text`
	font-style: normal;
	font-weight: bold;
	font-size: 16px;
	line-height: 20px;
	display: flex;
	align-items: center;
	text-align: center;

	color: #FFFFFF;
`
const SubmitButton = ({ title, onPress }) => {
	return (
	  <Button onPress={onPress}>
		<ButtonText>{title}</ButtonText>
	  </Button>
	);
};

export default SubmitButton;
