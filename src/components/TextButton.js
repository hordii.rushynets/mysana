import styled from 'styled-components/native';



const TextButton = styled.TouchableOpacity`
    position: absolute;
    width: 93px;
    height: 20px;
    left: 24px;
    top: 572px;

    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 20px;

    text-decoration-line: underline;

    color: #283547;
`;

export default TextButton;
