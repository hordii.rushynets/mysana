import styled from 'styled-components/native';

const AppNameText = styled.Text`
	font-style: normal;
	font-weight: bold;
	font-size: 22px;
	line-height: 27px;
	color: #3B7EE2;
`

export default AppNameText;